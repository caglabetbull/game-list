package com.caglacetin.gamelist.util

import com.caglacetin.gamelist.ui.GameItem

object DummyGameItem {
  fun createDummyGameItem(): GameItem {
    return GameItem(
      title = "Starz Megaways",
      imageUrl = "https://www.unibet.co.uk/polopoly_fs/1.1475925.1595494478!/image/2878552747.jpg"
    )
  }

  fun createDummyGameList(): List<GameItem> {
    val gameList = mutableListOf<GameItem>()
    gameList.add(createDummyGameItem())
    return gameList
  }

}
