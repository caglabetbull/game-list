package com.caglacetin.gamelist.util

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.caglacetin.gamelist.common.Resource
import com.caglacetin.gamelist.domain.FetchGames
import com.caglacetin.gamelist.ui.GameItem
import com.caglacetin.gamelist.ui.GameListViewModel
import com.caglacetin.gamelist.util.DummyGameItem.createDummyGameList
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.lang.Exception
import org.junit.Assert.assertEquals

@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class)
class GameListViewModelTest {
  @MockK
  lateinit var fetchGames: FetchGames

  // Set the main coroutines dispatcher for unit testing.
  @ExperimentalCoroutinesApi
  @get:Rule
  var mainCoroutineRule = MainCoroutineRule()

  // Executes each task synchronously using Architecture Components.
  @get:Rule
  var instantExecutorRule = InstantTaskExecutorRule()

  private lateinit var gameListViewModel: GameListViewModel

  @Before
  fun setUp() {
    MockKAnnotations.init(this)
    gameListViewModel = GameListViewModel(fetchGames)
  }

  @Test
  fun `given success state, when fetchImages called`() {
    val successStatus = Resource.Success(createDummyGameList())

    coEvery { fetchGames.fetchGames() } returns flow {
      emit(successStatus)
    }

    gameListViewModel.getGames()
    gameListViewModel.gamesLiveData.observeForever { }

    assertEquals(successStatus, gameListViewModel.gamesLiveData.value)
  }

  @Test
  fun `given error state, when fetchImages called`() {
    val exception = Resource.DataError(Exception())
    val error: Resource<List<GameItem>> = exception

    coEvery { fetchGames.fetchGames() } returns flow {
      emit(error)
    }

    gameListViewModel.getGames()
    gameListViewModel.gamesLiveData.observeForever { }

    assertEquals(exception, gameListViewModel.gamesLiveData.value)
  }

}
