package com.caglacetin.gamelist

import com.caglacetin.gamelist.ui.GameItemViewState
import com.caglacetin.gamelist.util.DummyGameItem.createDummyGameItem
import com.google.common.truth.Truth
import org.junit.Test

class GameItemViewStateTest {

  @Test
  fun `should match game image url for given game item`() {
    // Given
    val gameItem = createDummyGameItem()
    val givenViewState = GameItemViewState(gameItem)
    // When
    val actualResult = givenViewState.getGameImageUrl()
    // Then
    Truth.assertThat(actualResult).isEqualTo(gameItem.imageUrl)
  }
}
