package com.caglacetin.gamelist.ui

import com.caglacetin.gamelist.common.Resource

data class GameListViewState(
  private val status: Resource<Any>
) {
  fun isLoading() = status is Resource.Loading
  fun getErrorMessage() = if (status is Resource.DataError) status.exception else ""
  fun shouldShowErrorMessage() = status is Resource.DataError
}
