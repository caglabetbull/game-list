package com.caglacetin.gamelist.ui

data class GameItemViewState(
  val gameItem: GameItem
){

  fun getGameTitle() = gameItem.title
  fun getGameImageUrl() = gameItem.imageUrl
}
