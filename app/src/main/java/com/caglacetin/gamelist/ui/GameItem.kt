package com.caglacetin.gamelist.ui

data class GameItem(
  val imageUrl: String,
  val title: String
)
