package com.caglacetin.gamelist.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.caglacetin.gamelist.common.Resource
import com.caglacetin.gamelist.domain.FetchGames
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameListViewModel @Inject constructor(
  private val usaCase: FetchGames
): ViewModel() {

  private val gamesLiveDataPrivate = MutableLiveData<Resource<List<GameItem>>>()
  val gamesLiveData: LiveData<Resource<List<GameItem>>> get() = gamesLiveDataPrivate

  fun getGames() {
    viewModelScope.launch {
      gamesLiveDataPrivate.value = Resource.Loading
      usaCase.fetchGames().collect {
        gamesLiveDataPrivate.value = it
      }
    }
  }

}
