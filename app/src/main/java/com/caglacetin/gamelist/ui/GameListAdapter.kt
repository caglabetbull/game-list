package com.caglacetin.gamelist.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.caglacetin.gamelist.R
import com.caglacetin.gamelist.common.inflate
import com.caglacetin.gamelist.databinding.ItemGameBinding
import com.caglacetin.gamelist.ui.GameListAdapter.GameItemViewHolder
import javax.inject.Inject

class GameListAdapter @Inject constructor() : RecyclerView.Adapter<GameItemViewHolder>() {

  private val gameList: MutableList<GameItem> = mutableListOf()

  fun setGames(games: List<GameItem>) {
    gameList.addAll(games)
    notifyDataSetChanged()
  }

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): GameItemViewHolder {
    val itemBinding = parent.inflate<ItemGameBinding>(R.layout.item_game, false)
    return GameItemViewHolder(itemBinding)
  }

  override fun onBindViewHolder(
    holder: GameItemViewHolder,
    position: Int) {
    holder.bind(gameList[position])
  }

  override fun getItemCount(): Int = gameList.size

  inner class GameItemViewHolder(private val binding: ItemGameBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(gameItem: GameItem) {
      with(binding) {
        viewState = GameItemViewState(gameItem)
        executePendingBindings()
      }
    }
  }
}
