package com.caglacetin.gamelist.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.caglacetin.gamelist.common.Resource
import com.caglacetin.gamelist.common.Resource.DataError
import com.caglacetin.gamelist.common.Resource.Loading
import com.caglacetin.gamelist.common.Resource.Success
import com.caglacetin.gamelist.common.observe
import com.caglacetin.gamelist.databinding.ActivityMainBinding
import com.caglacetin.gamelist.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity: BaseActivity() {

  private lateinit var binding: ActivityMainBinding
  private val listViewModel: GameListViewModel by viewModels()

  @Inject
  internal lateinit var gameListAdapter: GameListAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    listViewModel.getGames()
    initRecyclerView()
  }

  private fun initRecyclerView() {
    val linearLayoutManager = LinearLayoutManager(this)
    binding.rvGames.apply {
      adapter = gameListAdapter
      layoutManager = linearLayoutManager
    }
  }

  override fun initViewBinding() {
    binding = ActivityMainBinding.inflate(layoutInflater)
    val view = binding.root
    setContentView(view)
  }

  override fun observeViewModel() {
    observe(listViewModel.gamesLiveData, ::setGameList)
  }

  private fun setGameList(status: Resource<List<GameItem>>) {
    binding.viewState = GameListViewState(status)
    when (status) {
      is Loading -> GameListViewState(Loading)
      is Success -> {
        GameListViewState(Success(status.data))
        status.data.let { gameListAdapter.setGames(it) }
      }
      is DataError -> GameListViewState(DataError(status.exception))
    }
  }
}
