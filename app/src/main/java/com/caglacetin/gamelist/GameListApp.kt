package com.caglacetin.gamelist

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class GameListApp: Application()