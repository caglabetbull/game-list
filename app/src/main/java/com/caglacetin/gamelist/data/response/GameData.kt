package com.caglacetin.gamelist.data.response

data class GameData(
  val gameId: String,
  val gameName: String,
  val imageUrl: String
)
