package com.caglacetin.gamelist.data.remote

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RequestInterceptor @Inject constructor() : Interceptor {

  override fun intercept(chain: Interceptor.Chain): Response {
    var request = chain.request()
    val httpUrl = request.url.newBuilder()
      .addQueryParameter(JURISDICTION, JURISDICTION_VALUE)
      .addQueryParameter(BRAND, BRAND_VALUE)
      .addQueryParameter(DEVICE, DEVICE_VALUE)
      .addQueryParameter(LOCALE, LOCALE_VALUE)
      .addQueryParameter(CURRENCY, CURRENCY_VALUE)
      .addQueryParameter(CATEGORIES, CATEGORIES_VALUE)
      .addQueryParameter(CLIENT_ID, CLIENT_ID_VALUE)
      .build()
    request = request.newBuilder().url(httpUrl).build()
    return chain.proceed(request)
  }

  companion object {
    const val JURISDICTION = "jurisdiction"
    const val JURISDICTION_VALUE = "UK"
    const val BRAND = "brand"
    const val BRAND_VALUE = "unibet"
    const val DEVICE = "deviceGroup"
    const val DEVICE_VALUE = "mobilephone"
    const val LOCALE = "locale"
    const val LOCALE_VALUE = "en_GB"
    const val CURRENCY = "currency"
    const val CURRENCY_VALUE = "GBP"
    const val CATEGORIES = "categories"
    const val CATEGORIES_VALUE = "casino,softgames"
    const val CLIENT_ID = "clientId"
    const val CLIENT_ID_VALUE = "casinoapp"
  }
}
