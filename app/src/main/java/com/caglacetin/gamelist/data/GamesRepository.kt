package com.caglacetin.gamelist.data

import com.caglacetin.gamelist.common.Resource
import com.caglacetin.gamelist.data.remote.RemoteDataRepo
import com.caglacetin.gamelist.data.response.GameResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class GamesRepository @Inject constructor(
  private val remoteRepository: RemoteDataRepo,
  private val ioDispatcher: CoroutineContext
) {

  suspend fun fetchGames(): Flow<Resource<GameResponse>> {
    return flow {
      emit(remoteRepository.fetchGames())
    }.flowOn(ioDispatcher)
  }

}
