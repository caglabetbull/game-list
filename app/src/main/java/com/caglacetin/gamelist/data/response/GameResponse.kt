package com.caglacetin.gamelist.data.response

data class GameResponse(
  val games: Map<String, GameData>
)
