package com.caglacetin.gamelist.data.remote

import com.caglacetin.gamelist.data.response.GameResponse
import retrofit2.Response
import retrofit2.http.GET

interface GameService {

  @GET("getGamesByMarketAndDevice.json")
  suspend fun fetchGames(): Response<GameResponse>

}
