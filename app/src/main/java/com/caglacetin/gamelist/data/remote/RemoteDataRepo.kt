package com.caglacetin.gamelist.data.remote

import com.caglacetin.gamelist.common.Resource
import com.caglacetin.gamelist.data.base.BaseDataSource
import com.caglacetin.gamelist.data.response.GameResponse
import javax.inject.Inject

class RemoteDataRepo @Inject constructor(
  private val service: GameService,
): BaseDataSource(), RemoteDataSource {

  override suspend fun fetchGames(): Resource<GameResponse> {
    return processCall {
      service.fetchGames()
    }
  }
}
