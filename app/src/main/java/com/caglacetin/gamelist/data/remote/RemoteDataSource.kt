package com.caglacetin.gamelist.data.remote

import com.caglacetin.gamelist.common.Resource
import com.caglacetin.gamelist.data.response.GameResponse

interface RemoteDataSource {
  suspend fun fetchGames(): Resource<GameResponse>
}
