package com.caglacetin.gamelist.common

interface Mapper<R, D> {
  fun mapFrom(type: R): D
}
