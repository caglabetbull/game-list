package com.caglacetin.gamelist.domain

import com.caglacetin.gamelist.common.Resource
import com.caglacetin.gamelist.common.map
import com.caglacetin.gamelist.data.GamesRepository
import com.caglacetin.gamelist.ui.GameItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class FetchGames  @Inject constructor(
  private val repository: GamesRepository,
  private val mapper: GamesMapper
) {
  suspend fun fetchGames(): Flow<Resource<List<GameItem>>> =
    repository.fetchGames().map { resource ->
      resource.map { response ->
        mapper.mapFrom(response)
      }
    }
}
