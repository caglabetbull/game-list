package com.caglacetin.gamelist.domain

import com.caglacetin.gamelist.common.Mapper
import com.caglacetin.gamelist.data.response.GameResponse
import com.caglacetin.gamelist.ui.GameItem
import javax.inject.Inject

class GamesMapper @Inject constructor() : Mapper<GameResponse, List<GameItem>> {

  override fun mapFrom(type: GameResponse): List<GameItem> =
    type.games.map { gameData ->
      GameItem(
        title = gameData.value.gameName,
        imageUrl = gameData.value.imageUrl
      )
    }
}
